#include <stdio.h>

void pattern(int i); //function to control the pattern
void Row(int j); //function to print each line of the pattern
int NoOfRows=1; //initial value for number of rows
int N; //variable to get the input

void pattern(int i)
{

    if(i>0)
    {
        Row(NoOfRows);
        printf("\n");
        NoOfRows++;
        pattern(i-1);

    }
}


void Row(int j)
{
    if(j>0)
    {
        printf("%d",j);
        Row(j-1);
    }
}

int main()
{

    printf("Enter the No. of Rows : ");
    scanf("%d",&N);
    pattern(N);
    return 0;
}
